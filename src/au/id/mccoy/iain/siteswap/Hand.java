package au.id.mccoy.iain.siteswap;

public class Hand implements Comparable {
	public static final Hand LEFT = new Hand(0);
	public static final Hand RIGHT = new Hand(1);
	
	private int v;
	
	private Hand(int v) {
		this.v = v;
	}

	public static Hand handFor(boolean leftHand) {
		return leftHand ? LEFT : RIGHT;
	}

	public Hand getOther() {
		if (this == LEFT)
			return RIGHT;
		else
			return LEFT;
	}

	public int compareTo(Object another) {
		if (another.getClass() != Hand.class)
			return -1;
		return new Integer(v).compareTo(new Integer(((Hand)another).v));
	}
}
