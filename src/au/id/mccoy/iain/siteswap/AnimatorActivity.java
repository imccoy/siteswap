package au.id.mccoy.iain.siteswap;

import java.text.ParseException;

import android.app.Activity;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

public class AnimatorActivity extends Activity {
	private Siteswap siteswap;
	private SiteswapState siteswapState;
	private HandMovements handMovements;
	private float time;
	private View view;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	try {
    		siteswap = new Siteswap(getIntent().getData().getSchemeSpecificPart());
    		handMovements = HandMovements.getStyle(getIntent().getData().getFragment());
    		siteswapState = new SiteswapState(siteswap, handMovements);
    		startAnimation();
    	} catch (ParseException e) {
    		Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG);
    		this.finish();
    	}
	}


	private void startAnimation() {
		view = new View(this) {
			@Override
			public void onDraw(Canvas canvas) {
		    	siteswapState.drawAll(new SiteswapDrawer(canvas, getWidth(), getHeight(), siteswap.maxThrowTime()), time);
		    }
		};
		
		this.setContentView(view);
		scheduleBumpTime();
	}


	private void bumpTime() {
		time += 0.05;
		if (time > 0.99) {
			time = 0;
			siteswapState.next();
		}
		view.invalidate();
		scheduleBumpTime();
	}


	private void scheduleBumpTime() {
		new Handler().postDelayed(new Runnable() { 
			public void run() {
				bumpTime(); 
			}
		}, 50);
	}
}
