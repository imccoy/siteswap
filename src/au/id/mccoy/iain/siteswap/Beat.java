package au.id.mccoy.iain.siteswap;

import java.util.List;

public abstract class Beat {

	public Beat() {
		super();
	}

	public abstract List<AirborneBallState> tossBalls(boolean leftHand, HandMovements handMovements);
	public abstract Beat mirror();
	
	public abstract int maxThrowTime();
}