package au.id.mccoy.iain.siteswap;

import java.text.ParseException;
import java.util.List;


public class Siteswap {

	final private List<Beat> beats;

	public Siteswap(String stringDescription) throws ParseException {
		beats = SiteswapParser.parse(stringDescription);
	}

	public int period() {
		return beats.size();
	}
	
	public Beat throwAt(int i) {
		return beats.get(i);
	}
	
	public int maxThrowTime() {
		int max = 0;
		for (Beat beat : beats) {
			max = Math.max(max, beat.maxThrowTime());
		}
		return max;
	}
}
