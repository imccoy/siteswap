package au.id.mccoy.iain.siteswap;

import java.util.ArrayList;
import java.util.List;

public class ThrowList {
	private ArrayList<Throw> tosses;
	
	public ThrowList(Throw height) {
		tosses = new ArrayList<Throw>();
		tosses.add(height);
	}
	
	public ThrowList(ArrayList<Throw> tosses) {
		this.tosses = tosses;
	}

	public List<AirborneBallState> tossBallsFrom(Hand hand, HandMovements handMovements) {
		List<AirborneBallState> ballStates = new ArrayList<AirborneBallState>();
		for (Throw toss : tosses) {
			ballStates.add(AirborneBallState.startWithHeight(toss, hand, handMovements));
		}
		return ballStates;
	}

	public int maxThrowTime() {
		int max = 0;
		for (Throw toss : tosses) {
			max = Math.max(max, toss.height);
		}
		return max;
	}
}
