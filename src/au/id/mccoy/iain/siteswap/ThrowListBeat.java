package au.id.mccoy.iain.siteswap;

import java.util.List;

public class ThrowListBeat extends Beat {
	private ThrowList tosses;

	public ThrowListBeat(ThrowList throwList) {
		tosses = throwList;
	}

	public ThrowList getHeights() {
		return tosses;
	}

	@Override
	public List<AirborneBallState> tossBalls(boolean leftHand, HandMovements handMovements) {
		return tosses.tossBallsFrom(Hand.handFor(leftHand), handMovements);
	}

	@Override
	public Beat mirror() {
		return this;
	}

	@Override
	public int maxThrowTime() {
		return tosses.maxThrowTime();
	}

}
