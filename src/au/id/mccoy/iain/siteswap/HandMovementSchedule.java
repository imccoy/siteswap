package au.id.mccoy.iain.siteswap;

import java.util.List;

public class HandMovementSchedule {
	private List<Point> points;
	private int offset;
	
	public HandMovementSchedule(List<Point> points) {
		this.points = points;
		this.offset = 0;
	}
	
	private HandMovementSchedule(List<Point> points, int offset) {
		this(points);
		this.offset = offset;
	}
	
	public HandMovementSchedule offsetBy(int offset) {
		return new HandMovementSchedule(points, offset);
	}
	
	public Point positionAt(int t) {
		while (t < 0)
			t += points.size();
		return points.get((t + offset) % points.size());
	}
}
