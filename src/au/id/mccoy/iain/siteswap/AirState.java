package au.id.mccoy.iain.siteswap;

import java.util.ArrayList;
import java.util.List;

public class AirState {
	private final List<BallState> ballStates;
	private boolean leftHand;

	public AirState() {
		this.ballStates = new ArrayList<BallState>();
		this.leftHand = false;
	}
	
	private AirState(List<BallState> ballStates, boolean leftHand) {
		this.ballStates = ballStates;
		this.leftHand = leftHand;
	}
	
	public AirState nextBeat(Beat beat, HandMovements handMovements) {
		ArrayList<BallState> newBallStates = new ArrayList<BallState>();
		for (BallState ballState : ballStates) {
			addIfNotNull(newBallStates, ballState.nextBeat());
		}
		return new AirState(newBallStates, !leftHand).withNewBalls(beat, handMovements);
	}
	
	private AirState withNewBalls(Beat beat, HandMovements handMovements) {
		List<BallState> newBallStates = new ArrayList<BallState>(ballStates);
		for (AirborneBallState ballState : beat.tossBalls(leftHand, handMovements)) {
			addIfNotNull(newBallStates, ballState);
		}
		return new AirState(newBallStates, leftHand);
	}

	public void drawAll(SiteswapDrawer d, float t) {
		for (BallState ballState : ballStates) {
			ballState.draw(d, t);
		}
	}

	private void addIfNotNull(List<BallState> ballStates, BallState ballState) {
		if (ballState != null)
			ballStates.add(ballState);
	}
}