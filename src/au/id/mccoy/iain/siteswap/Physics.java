package au.id.mccoy.iain.siteswap;

public class Physics {
	public static final float G = -10;

	static float calculateInitialVelocity(float distance, float acceleration, float time) {
		return (float)(distance - 0.5 * acceleration * Math.pow(time, 2)) / time;
	}
	
	static float calculateMaxHeight(float y0, float y1, float time) {
		float initialVelocity = calculateInitialVelocity(y1 - y0, G, time);
		return y0 + calculateDistanceMoved(initialVelocity, G, time / 2); 
	}
	
	static float calculateDistanceMoved(float initialVelocity, float acceleration, float time) {
		return (float)(initialVelocity * time + 0.5 * acceleration * Math.pow(time, 2));
	}
}
