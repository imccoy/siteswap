package au.id.mccoy.iain.siteswap;

public class Throw {
	int height;
	boolean crossing;
	
	
	public Throw(int height, boolean crossing) {
		this.height = height;
		this.crossing = crossing;
	}
	
	boolean getCrossing() {
		return crossing || (height % 2 != 0);
	}

	int getHeight() {
		return height;
	}
}
