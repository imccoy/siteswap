package au.id.mccoy.iain.siteswap;

import java.util.ArrayList;
import java.util.List;

public class SyncBeat extends Beat {
	private List<ThrowList> throwLists;
	
	public SyncBeat() {
		this.throwLists = new ArrayList<ThrowList>();
	}
	
	public SyncBeat(List<ThrowList> hands) {
		this.throwLists = hands;
	}
	
	@Override
	public List<AirborneBallState> tossBalls(boolean unused, HandMovements handMovements) {
		List<AirborneBallState> ballStates = new ArrayList<AirborneBallState>();
		boolean leftHand = true;
		for (ThrowList hand : throwLists) {
			ballStates.addAll(hand.tossBallsFrom(Hand.handFor(leftHand), handMovements));
			leftHand = !leftHand;
		}
		
		return ballStates;
	}

	@Override
	public Beat mirror() {
		List<ThrowList> newHands = new ArrayList<ThrowList>();
		for (int i = 0; i < throwLists.size(); i += 2) {
			if (i + 1 >= throwLists.size()) {
				newHands.add(throwLists.get(i));
			} else {
				newHands.add(throwLists.get(i+1));
				newHands.add(throwLists.get(i));
			}
		}
		return new SyncBeat(newHands);
	}

	@Override
	public int maxThrowTime() {
		int max = 0;
		for (ThrowList hand : throwLists) {
			max = Math.max(max, hand.maxThrowTime());
		}
		return max;
	}
}
