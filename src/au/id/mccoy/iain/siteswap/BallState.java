package au.id.mccoy.iain.siteswap;

public abstract class BallState {
	
	public abstract BallState nextBeat();
	public abstract void draw(SiteswapDrawer d, float t);

}