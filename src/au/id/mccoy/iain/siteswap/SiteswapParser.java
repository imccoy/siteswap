package au.id.mccoy.iain.siteswap;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SiteswapParser {
	private String stringDescription;
	private int point = 0;
	
	public static List<Beat> parse(String stringDescription) throws ParseException {
		SiteswapParser parser = new SiteswapParser(stringDescription); 
		return parser.parse();
	}
	
	private SiteswapParser(String stringDescription) {
		this.stringDescription = stringDescription;
		this.point = 0;
	}
	
	private List<Beat> parse() throws ParseException {	
		List<Beat> beats = new ArrayList<Beat>();
		while (point < stringDescription.length()) {
			if (thisChar() == '(') {
				beats.add(parseSyncBeat());
				beats.add(new SyncBeat());
			} else if (thisChar() == '*') {
				beats.addAll(mirror(beats));
			} else {
				beats.add(parseSimpleBeat());
			}
		}
		return beats;
	}

	private Collection<? extends Beat> mirror(List<Beat> beats) throws ParseException {
		List<Beat> mirrored = new ArrayList<Beat>();
		expect('*');
		for (Beat beat : beats) {
			mirrored.add(beat.mirror());
		}
		return mirrored;
	}

	private SyncBeat parseSyncBeat() throws ParseException {
		List<ThrowList> throwLists = new ArrayList<ThrowList>();
		expect('(');
		while (thisChar() != ')') {
			throwLists.add(parseThrowList());
			if (thisChar() == ',')
				expect(',');
		}
		expect(')');
		return new SyncBeat(throwLists);		
	}

	private ThrowListBeat parseSimpleBeat() throws ParseException {
		return new ThrowListBeat(parseThrowList());
	}

	private ThrowList parseThrowList() throws ParseException {
		if (thisChar() == '[') {
			return parseMultiplexThrowList();
		} else {
			return parseSimpleThrowList();
		}
	}
	
	private char thisChar() {
		if (point < stringDescription.length())
			return stringDescription.charAt(point);
		else
			return '\0';
	}

	private ThrowList parseSimpleThrowList() throws ParseException {
		return new ThrowList(parseSimpleThrow());
	}

	private ThrowList parseMultiplexThrowList() throws ParseException {
		ArrayList<Throw> tosses = new ArrayList<Throw>(); 
		expect('[');
		while (thisChar() != ']') {
			tosses.add(parseSimpleThrow());
		}
		expect(']');
		return new ThrowList(tosses);
	}

	private Throw parseSimpleThrow() throws ParseException {
		int height = parseThrowHeight();
		boolean crossing = false;
		point += 1;
		if (thisChar() == 'x') {
			crossing = true;
			point += 1;
		}
		return new Throw(height, crossing);
	}

	private int parseThrowHeight() throws ParseException {
		try {
			return Integer.parseInt(Character.toString(thisChar()));
		} catch (Exception e) {
			throw new ParseException("Not a valid throw height " + thisChar(), point);
		}
	}

	private void expect(char c) throws ParseException {
		if (thisChar() != c) {
			throw new ParseException("Got " + thisChar() + ", but expected " + c, point);
		}
		point += 1;
	}

}
