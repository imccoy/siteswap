package au.id.mccoy.iain.siteswap;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import com.google.common.collect.Lists;

public class HandMovements {
	private static final Map<String, HandMovements> styles = buildStyles();
	
	private final Map<Hand, HandMovementSchedule> map;
	private final int time;
	
	public HandMovements(HandMovementSchedule left, HandMovementSchedule right) {
		map = new TreeMap<Hand, HandMovementSchedule>();
		map.put(Hand.LEFT, left);
		map.put(Hand.RIGHT, right);
		time = 0;
	}
	
	private static Map<String, HandMovements> buildStyles() {
		Map<String, HandMovements> styles = new HashMap<String,HandMovements>();
		Point lh_in = new Point(-7,0), lh_out = new Point(-10,0);
		Point rh_in = new Point(7, 0), rh_out = new Point(10, 0);
		styles.put("Cascade", new HandMovements(
				new HandMovementSchedule(Lists.newArrayList(lh_in, lh_out)),
				new HandMovementSchedule(Lists.newArrayList(rh_out, rh_in))
				));
		styles.put("Reverse Cascade", new HandMovements(
				new HandMovementSchedule(Lists.newArrayList(lh_out, lh_in)),
				new HandMovementSchedule(Lists.newArrayList(rh_in, rh_out))
				));
		styles.put("Fountain", new HandMovements(
				new HandMovementSchedule(Lists.newArrayList(lh_in, lh_out)),
				new HandMovementSchedule(Lists.newArrayList(rh_in, rh_out))
				));
		styles.put("Reverse Fountain", new HandMovements(
				new HandMovementSchedule(Lists.newArrayList(lh_out, lh_in)),
				new HandMovementSchedule(Lists.newArrayList(rh_out, rh_in))
				));
		return styles;
	}
	
	public static HandMovements getStyle(String name) {
		return styles.get(name);
	}

	private HandMovements(Map<Hand, HandMovementSchedule> map, int time) {
		this.map = map;
		this.time = time;
	}
	
	public HandMovements next() {
		return next(1);
	}
	
	public HandMovements next(int height) {
		return new HandMovements(map, time + height);
	}
	
	public Point position(Hand hand) {
		return map.get(hand).positionAt(time);
	}

	public Point position(Hand hand, int i) {
		return map.get(hand).positionAt(time + i);
	}
}
