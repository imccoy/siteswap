package au.id.mccoy.iain.siteswap;

class Path {
	private final Hand source;
	private final Hand destination;
	private final int height;
	private final HandMovements handMovements;
	
	Path(Hand source, Hand destination, int height, HandMovements handMovements) {
		if (source == null) throw new NullPointerException("Can't have null source");
		if (destination == null) throw new NullPointerException("Can't have null destination");
		this.source = source;
		this.destination = destination;
		this.height = height;
		this.handMovements = handMovements;
	}
	
	public Hand getDestination() {
		return destination;
	}
	
	public int getHeight() {
		return height;
	}
	
	public HandMovements getHandMovements() {
		return handMovements;
	}

	public int startX() {
		return handMovements.position(source).getX();
	}

	public int endX() {
		return handMovements.position(destination, height - 1).getX();
	}
}