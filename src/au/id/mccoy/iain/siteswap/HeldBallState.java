package au.id.mccoy.iain.siteswap;

import android.util.Log;

public class HeldBallState extends BallState {

	private final Hand hand;
	private final HandMovements handMovements;

	public HeldBallState(Hand destination, HandMovements handMovements) {
		hand = destination;
		this.handMovements = handMovements;
	}

	@Override
	public void draw(SiteswapDrawer d, float t) {
		int startX = handMovements.position(hand).getX();
		int endX = handMovements.position(hand, 1).getX();
		d.draw(startX + (endX - startX) * t, 0);
	}

	@Override
	public BallState nextBeat() {
		return null;
	}

}
