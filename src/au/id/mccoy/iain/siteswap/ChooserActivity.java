package au.id.mccoy.iain.siteswap;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class ChooserActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chooser);
        
        Button start_animation = (Button)this.findViewById(R.id.start_animation);
        start_animation.setOnClickListener(new OnClickListener() {

			public void onClick(View button) {
				EditText siteswap_input = (EditText)ChooserActivity.this.findViewById(R.id.siteswap_input);
				startAnimating(siteswap_input.getText().toString());
			}
        	
        });
        assignInsertCharacterListener(R.id.openparen, '(');
        assignInsertCharacterListener(R.id.closeparen, ')');
        assignInsertCharacterListener(R.id.opensquare, '[');
        assignInsertCharacterListener(R.id.closesquare, ']');
        assignInsertCharacterListener(R.id.asterisk, '*');
        assignInsertCharacterListener(R.id.comma, ',');
        
        loadHandMovementStyles();
    }
    
    private void loadHandMovementStyles() {
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.movement_styles, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		movementStyleSelector().setAdapter(adapter);
	}

	private Spinner movementStyleSelector() {
		Spinner spinner = (Spinner) findViewById(R.id.select_movement_style);
		return spinner;
	}

	private void assignInsertCharacterListener(int resourceID, char c) {
		Button button = (Button)this.findViewById(resourceID);
		button.setOnClickListener(new InsertCharacterListener(c));
	}

	private void startAnimating(String siteswap_input) {
    	Intent intent = new Intent();
    	intent.setAction(Intent.ACTION_VIEW);
    	intent.setData(Uri.fromParts("siteswap", siteswap_input, selectedMovementStyle()));
    	intent.setClass(this, AnimatorActivity.class);
    	this.startActivity(intent);
    }
    
    private String selectedMovementStyle() {
		return (String)this.movementStyleSelector().getSelectedItem();
	}

	private class InsertCharacterListener implements OnClickListener {
    	private char characterToInsert;

		public InsertCharacterListener(char characterToInsert) {
    		this.characterToInsert = characterToInsert;
    	}
    	
    	public void onClick(View v) {
    		TextView siteswap_input = (TextView)ChooserActivity.this.findViewById(R.id.siteswap_input);
    		siteswap_input.append(Character.toString(characterToInsert));
    	}    	
    }

}