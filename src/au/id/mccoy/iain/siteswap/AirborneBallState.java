package au.id.mccoy.iain.siteswap;


public class AirborneBallState extends BallState {
	private final int progress;
	private final Path path;
	
	private AirborneBallState(Path path, int progress) {
		this.progress = progress;
		this.path = path;
	}
	
	public BallState nextBeat() {
		int nextProgress = progress + 1;
		if (nextProgress < airtime()) {
			return new AirborneBallState(path, nextProgress);
		} else if (path.getHeight() != 1) {
			return new HeldBallState(path.getDestination(), path.getHandMovements().next(path.getHeight() - 1));
		} else {
			return null;
		}
		
	}

	public static AirborneBallState startWithHeight(Throw toss, Hand source, HandMovements handMovements) {
		boolean crossing = toss.getCrossing();
		Path p = new Path(source, crossing ? source.getOther() : source, toss.getHeight(), handMovements);
		return new AirborneBallState(p, 0);
	}

	@Override
	public void draw(SiteswapDrawer d, float t) {
		float t1 = progress + t;
		float x = x(t1);
		float y = y(t1);
		d.draw(x, y);
	}
	
	private float y(float t) {
		// y = v0 * t - 0.5 * g * t * t, v0 is starting velocity
		// choose v0 so that at t = (height - 0.5), y is 0
		float v0 = Physics.calculateInitialVelocity(0, Physics.G, airtime());
		return Math.max(0, Physics.calculateDistanceMoved(v0, Physics.G, t));
	}

	private float x(float t) {
		int startX = path.startX();
		int endX = path.endX();
		return startX + (endX - startX) * (t / airtime());
	}
	
	private int airtime() {
		return path.getHeight() == 1 ? 1 : path.getHeight() - 1;
	}
	

}
