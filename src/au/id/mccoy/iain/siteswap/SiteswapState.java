package au.id.mccoy.iain.siteswap;

import com.google.common.collect.Lists;


public class SiteswapState {
	private Siteswap siteswap;
	private AirState airState;
	private HandMovements handMovements;
	private int t;
	
	public SiteswapState(Siteswap siteswap, HandMovements handMovements) {
		this.siteswap = siteswap;
		this.airState = new AirState();
		this.handMovements = handMovements; 
		t = -1;
		next();
	}
	
	public void next() {
		t = (t + 1) % siteswap.period();
		airState = airState.nextBeat(siteswap.throwAt(t), handMovements);
		handMovements = handMovements.next();
	}
	
	public void drawAll(SiteswapDrawer drawer, float t) {
		airState.drawAll(drawer, t);
	}
	

}
