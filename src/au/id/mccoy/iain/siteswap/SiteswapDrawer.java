package au.id.mccoy.iain.siteswap;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.util.Log;

public class SiteswapDrawer {

	private Canvas canvas;
	private ShapeDrawable drawable;
	private int width;
	private int height;
	private float worldHeight;

	public SiteswapDrawer(Canvas canvas, int width, int height, int maxThrowTime) {
		this.canvas = canvas;
		this.drawable = new ShapeDrawable(new OvalShape());
		this.width = width;
		this.height = height;
		this.worldHeight = Physics.calculateMaxHeight(0, 0, maxThrowTime);
		
		drawable.getPaint().setColor(Color.WHITE);
		canvas.drawRect(new Rect(0, 0, width, height), drawable.getPaint());
		drawable.getPaint().setColor(Color.RED);
	}

	public void draw(float x, float y) {
		final int size = 25;
		float cx = (x + 10) * ((width - size) / 10) / 2;
		float cy = height - (y / worldHeight) * height;
		drawable.setBounds((int)cx, (int)cy-size, (int)cx + size, (int)cy);
		drawable.draw(canvas);
	}

}
